import Vue from 'vue'
import Vuex from 'vuex'

// import { createPersistedState } from 'vuex-electron'

// import modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    // createPersistedState()
    // createSharedMutations()
  ],
  strict: process.env.NODE_ENV !== 'production',
  state: {
    socket: {
      isConnected: false,
      reconnectError: false
    },
    messages: [],
    users: []
  },
  getters: {
    // ...
    getUserById: state => id => {
      return state.users.find(user => user.id === id)
    }
  },
  mutations: {
    clear (state) {
      state.users = []
      state.messages = []
    },
    updateUser (state, user) {
      let index = state.users.findIndex(item => item.id === user.id)
      if (index >= 0) {
        state.users.splice(index, 1)
      }
      state.users.push(user)
    },
    SOCKET_ONOPEN (state, event) {
      Vue.prototype.$socket = event.currentTarget
      state.socket.isConnected = true
    },
    SOCKET_ONCLOSE (state, event) {
      state.socket.isConnected = false
    },
    SOCKET_ONERROR (state, event) {
      console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE (state, message) {
      for (let item of JSON.parse(message.data)) {
        item['date'] = new Date(item.start_time)
        item['uuid'] = state.messages.length
        state.messages.push(item)
      }
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT (state, count) {
      console.info(state, count)
    },
    SOCKET_RECONNECT_ERROR (state) {
      state.socket.reconnectError = true
    }
  },
  actions: {
  }
})
