import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import VueNativeSock from 'vue-native-websocket'

Vue.use(VueNativeSock, 'ws://localhost:8765', { store: store })

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */

const setIgnoreMouseEvents = require('electron').remote.getCurrentWindow().setIgnoreMouseEvents
addEventListener('pointerover', function mousePolicy (event) {
  mousePolicy._canClick = event.target === document.documentElement
    ? mousePolicy._canClick && setIgnoreMouseEvents(true, {forward: true})
    : mousePolicy._canClick || setIgnoreMouseEvents(false) || 1
})
setIgnoreMouseEvents(true, {forward: true})

new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
