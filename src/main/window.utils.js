import { screen } from 'electron'

let dy = 50
let minHeigth = 87

export const miniDisplayPrefs = () => {
  let display = screen.getPrimaryDisplay()
  let height = display.workArea.height
  return {
    yPosition () {
      return height - minHeigth
    },
    height () {
      return minHeigth
    },
    width () {
      // add development sizes
      return process.env.NODE_ENV === 'development' ? 1000 : 420
    }
  }
}

export const fullDisplayPrefs = () => {
  let display = screen.getPrimaryDisplay()
  let height = display.workArea.height

  return {
    yPosition () {
      return dy
    },
    height () {
      return height - dy
    },
    width () {
      // add development sizes
      return process.env.NODE_ENV === 'development' ? 1000 : 420
    }
  }
}
